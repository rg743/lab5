package wordcountextended;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class ExecutorCountLine extends Thread {
    
    private String _file;
    private Map<String, Integer> _lineCount;

    public ExecutorCountLine (String fileName){
        _file = fileName;
        _lineCount = new HashMap<String, Integer>();
    }
    
    public Map<String, Integer> lineCount(){
        return _lineCount;
    }
       
    @Override
    public void run() {
        try{
            BufferedReader in = new BufferedReader(new FileReader(_file));
            int streamLine = 0;
            while (in.readLine() != null) streamLine++;
            _lineCount.put(_file, streamLine);
            in.close();
        }
        catch(Exception e) {
            System.out.print("Error: " + e);
        }
    }
    
}