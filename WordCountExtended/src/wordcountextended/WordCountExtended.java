package wordcountextended;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WordCountExtended extends Thread{
    
    public static void main(String[] args) {
        int numFiles;
        
        numFiles = args.length;
        if (numFiles == 0){
           System.out.println("No files found.");
           System.exit(0);
        }
        else if (numFiles != 0){
           System.out.println("The following are files available for word count: ");
           for(int i = 0; i  < args.length; ++i ){
               System.out.println(args[i]);
           }
        }
            
        //Setting a Word and a Line array list
        List <Map<String, Integer>> wordCount = new ArrayList<Map<String, Integer>>();
        List <Map<String, Integer>> lineCount = new ArrayList<Map<String, Integer>>();
        
        //Creating Threadpool of 3
        ExecutorService executor = Executors.newFixedThreadPool(3);
        
        //Submitting Runnable tasks to the Executor
        ArrayList<Executor> threadWord = new ArrayList <Executor> ();               
        for (int i = 0; i < args.length; ++i){
            try {
                Executor tw = new Executor(args[i]);
                threadWord.add(tw);
                tw.run();
            }
            catch(Exception e){
                System.out.print("Error: " + e);
            }
        }
        
        //Submitting Runnable tasks to the ExecutortCountLine
        ArrayList<ExecutorCountLine> threadLine = new ArrayList <ExecutorCountLine> ();
        for (int i = 0; i < args.length; ++i){
            try{
                ExecutorCountLine tl = new ExecutorCountLine(args[i]);
                threadLine.add(tl);
                tl.run();
            }
            catch (Exception e){
                System.out.print("Error: " + e);
            }
        }
        
        //Taking the WordCounts from the Executor to store them in wordCount List.
        for ( Executor tw: threadWord){
            try {
                wordCount.add (tw.wordCount() );
            }
            catch(Exception e){
                System.out.println("Error: " + e);
            }
        }
        
        //Taking the lineCount from the Executor to store them in lineCount List.
        for (ExecutorCountLine tl: threadLine){
            try {
                lineCount.add (tl.lineCount());
            }
            catch (Exception e){
                System.out.println("Error: " + e);
            }
        }
        
        //Getting result from class and setting a Map to be displayed
        Map<String, Integer> totalWordCount = new HashMap<String, Integer> ();
        for ( Map<String, Integer> count: wordCount ) {
            for (Map.Entry<String, Integer> entry : count.entrySet()){
                String iword = entry.getKey();
                Integer icount = entry.getValue();
                totalWordCount.put(iword, icount);                
            }            
        }
        
        //Getting results from class ExecutorCountLine and setting a Map to be displayed
        Map<String, Integer> totalLineCount = new HashMap<String, Integer> ();
        for (Map<String, Integer> count : lineCount ){
            for(Map.Entry<String, Integer> entry : count.entrySet()){
                String jfile = entry.getKey();
                Integer jcount = entry.getValue();
                totalLineCount.put (jfile, jcount);
            }
        }
        
        //Displaying output result
        System.out.println("** Displaying the total word count **");
        for (Map.Entry<String, Integer> entry : totalWordCount.entrySet() ){
            System.out.println(entry.getKey() + ": \t" + entry.getValue());
        }
        
        System.out.println("** Displaying the total line count **");
        for (Map.Entry<String, Integer> entry : totalLineCount.entrySet() ){
            System.out.println(entry.getKey() + ": \t" + entry.getValue());
        }
        
        //Shutting down executor to destroy service.
        executor.shutdown();        
    }    
}