package wordcountextended;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;

public class Document 
{
    private String _name;
    private int _wordCount;
    private int _lineCount;
    private Map<String, Integer> _text;
    private Lock countChangeLock;
    private Condition countReady;
    
    public Document (String documentName) 
    {
        _name = documentName;
        _text = new HashMap<String, Integer> ();
        _wordCount = 0;
        _lineCount = 0;
        countChangeLock = new ReentrantLock();
        countReady = countChangeLock.newCondition();
    }
    
    public void countWords (String name, int wordCount)
    {
        countChangeLock.lock();       
        try
        {
            BufferedReader in = new BufferedReader(new FileReader(_name));
            String streamLine;
            while ((streamLine = in.readLine()) != null){
                for (String word: streamLine.split("\\s+")){ //"\\s+" is an expression for space. Here we are spliting the stream line by spaces/whitespaces (\\s+) meaning that we are extracting words from the streamline.
                    word = word.replaceAll("[^a-zA-Z]*", ""); //https://regexr.com/ Replacing all symbols and characters with "" except from a to z, A to Z and * is a quantifier
                                       
                        if(_text.get(word) != null) //This if statement can be shortened to _wordCount.put(word, totalCount == null ? 1 : totalCount + 1);
                        {
                            _wordCount = _wordCount + 1;
                        }
                        else
                        {
                            _wordCount = _wordCount;
                        }
                    _text.put(_name, _wordCount);
                }
            }
            in.close();
            
            System.out.println("Word Count for file: " + _name + " is " + _wordCount);
            
            countReady.signalAll();
        }
        catch (Exception e){
            System.out.println("Error: " + e);
        }
        finally
        {
            countChangeLock.unlock();
        }
    }
    
    public void countLine (String name, int lineCount)
    {           
        countChangeLock.lock();
        try
        {
            BufferedReader in = new BufferedReader(new FileReader(_name));
            int streamLine = 0;
            while (in.readLine() != null) streamLine++;
            _text.put(_name, _lineCount);
            in.close();
            
            System.out.println("Line Count for file: " + name + " is " + lineCount);
            
            countReady.signalAll();
        }
        catch (Exception e){
            System.out.println("Error: " + e);
        }
        finally
        {
            countChangeLock.unlock();
        }
    }
}
