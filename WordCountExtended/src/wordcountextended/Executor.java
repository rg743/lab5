package wordcountextended;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

class Executor extends Thread {
    
    private String _file;
    private Map<String, Integer> _wordCount;
    private int _totalCount;
    
    public Executor (String fileName){
        _file = fileName;
        _wordCount = new HashMap<String, Integer>();
    }
    
    public Map<String, Integer> wordCount(){
        return _wordCount;
    }
    
    public void run()
    {
        try {
            /*
            FileReader reader = new FileReader(_file);
            BufferedReader in = new BufferedReader(reader);
            */
            BufferedReader in = new BufferedReader(new FileReader(_file));
            String streamLine;
            while ((streamLine = in.readLine()) != null){
                for (String word: streamLine.split("\\s+")){ //"\\s+" is an expression for space. Here we are spliting the stream line by spaces/whitespaces (\\s+) meaning that we are extracting words from the streamline.
                    word = word.replaceAll("[^a-zA-Z]*", ""); //https://regexr.com/ Replacing all symbols and characters with "" except from a to z, A to Z and * is a quantifier
                                       
                        if(_wordCount.get(word) == null){ //This if statement can be shortened to _wordCount.put(word, totalCount == null ? 1 : totalCount + 1);
                            _totalCount = _totalCount + 1;
                        }
                        else{
                            _totalCount = _totalCount + 1;
                        }
                    _wordCount.put(_file, _totalCount);
                }
            }
            in.close();
        }
        catch (Exception e) {
            System.out.print("Error: " + e);
        }
    }    
}
